<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookBab extends Model
{
    protected $table = 'book_bab';
    protected $fillable = [
        'name', 'price', 'file_path','book_id' ,'created_at', 'updated_at'
    ];

    public function books()
    {
        return $this->belongsTo(Books::class, 'book_id');
    }

    public function order()
    {
        return $this->hasMany(Order::class, 'book_bab_id');
    }
}
