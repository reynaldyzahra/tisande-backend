<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'order';
    protected $fillable = [
        'id_order', 'book_bab_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function book_bab()
    {
        return $this->belongsTo(BookBab::class, 'book_bab_id');
    }
}
