<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topup extends Model
{
    protected $table = 'topup';
    protected $fillable = [
        'user_id','nominal', 'img_path', 'status'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
