<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->book_bab->id,
            'name' => $this->book_bab->books->name,
            'sinopsis' => $this->book_bab->books->sinopsis,
            'img_path' => $this->book_bab->books->img_path,
            'bab' => new BookBabResource($this->book_bab),
        ];
    }
}
