<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TopupResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'nominal' => $this->nominal, 
            'img_path' => $this->img_path, 
            'status' => $this->status,
            'user' => $this->user
        ];
    }
}
