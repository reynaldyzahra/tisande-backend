<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BookResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name, 
            'sinopsis' => $this->sinopsis,  
            'img_path' => url('/') . $this->img_path,
            'book_bab' => BookBabResource::collection($this->book_bab),
            'category' => $this->category
        ];
    }
}
