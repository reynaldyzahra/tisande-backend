<?php

namespace App\Http\Controllers;
use App\Topup;
use Illuminate\Http\Request;

class TopupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $topup = Topup::with('user')->get();
        return view('adm.topups.index', ['topups' => $topup]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $topup = Topup::with('user')->get();
        return view('adm.topups.create', ['topups' => $topup]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function accept($id)
    {
        $topup = Topup::where('id', $id)->first();
        if ($topup) {
            if ($topup->status == 'Pending' || $topup->status == 'Rejected') {

                //Update status topup ke sukses
                $topup->update([
                    'status' => 'Success'
                ]);

                //Tambah saldo user
                $topup->user->profile()->update([
                    'saldo' => ($topup->user->profile->saldo + $topup->nominal)
                ]);
    
                return redirect()->route('topups.index')
                        ->with('alert-success', 'Successfully accept topup');
            } else {
               return redirect()->route('topups.index')
                        ->with('alert-success', 'This topups already accepted');
            }
        }
 
    }

    public function reject($id)
    {
        $topup = Topup::where('id', $id)->first();
        if ($topup) {
            if ($topup->status == 'Pending') {

                //Update status topup ke sukses
                $topup->update([
                    'status' => 'Rejected'
                ]);

                //Tambah saldo user
                $topup->user->profile()->update([
                    'saldo' => ($topup->user->profile->saldo + $topup->nominal)
                ]);
    
                return redirect()->route('topups.index')
                        ->with('alert-success', 'Successfully reject topup');
            } else {
               return redirect()->route('topups.index')
                        ->with('alert-success', 'This topups already rejected');
            }
        }
 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
