<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Profile;

class UserController extends Controller
{
    public function postLogin(Request $request) {
        try {
            $auth = User::where(['username' => $request->username, 'password' => $request->password])->with('Profile')->first();
            if (!$auth) {
                return response()->json([
                    'status' => false,
                    'err_msg' => 'Couldn\'t find your account'
                ], 401);
            }else{
                return response()->json([
                    'status' => true,
                    'data' => $auth
                ]);
            }
        } catch (\Exception $err) {
            return response()->json([
                'status' => false,
                'err_msg' => 'Not Acceptable'
            ], 422);
        }
    }

    public function postRegister(Request $request) {
        try {
            $generateId = rand(0, 999999);
            $user = new User;
            $profile = new Profile;
            
            $user->id = $generateId;
            $user->username = $request->username;
            $user->password = $request->password;
            
            $profile->user_id = $generateId;
            $profile->name = $request->name;
            $profile->saldo = 0;
            $profile->photo_profile = 'google.com';
            
            $usernameExists = User::where(['username' => $request->username])->get();
            if ($usernameExists) {
                return response()->json([
                    'status' => false,
                    'err_msg' => 'Username already exists'
                ]);
            } else if ($user->save() && $profile->save()) {
                return response()->json([
                    'status' => true,
                    'err_msg' => 'Registration successfully'
                ], 422);
            }else{
                return response()->json([
                    'status' => false,
                    'err_msg' => 'Registration failed, try again later'
                ], 400);
            }
        } catch (\Exception $err) {
            return response()->json([
                'status' => false,
                'err_msg' => 'Not Acceptable'
            ], 422);
        }
    }
}
