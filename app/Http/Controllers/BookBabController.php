<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BookBab;
use App\Books;
use Illuminate\Support\Str;
class BookBabController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
        $bookbab = BookBab::all();

        return view('adm.bookbab.index', ['bookBab' => $bookbab]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bookbab = BookBab::all();
        $books = Books::all();
        // dd($books);
        return view('adm.bookbab.create', [
            'books'=> $bookbab,
            'buku' => $books
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'book_id' => 'required|string',
            'name' => 'required|string',
            'price' => 'required|string',
            'file_path' => 'required'
            
        ]);
            
        if ($request->has('file_path')) {
            $file = $request->file('file_path');
            $imagePath = '/file/books/';
            $path = public_path() . $imagePath;
            $extension = $file->getClientOriginalExtension();

            //Filename
            $filename = 'books-'.Str::random(16) . '.' . $extension;
            $file->move($path, $filename);

            $bookbab = BookBab::create([
                'book_id' => $request->book_id,
                'name' => $request->name,
                'file_path' => $imagePath . $filename,
                'price' => $request->price
            ]);

            if ($bookbab) {
                return redirect()->route('bookbab.index');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $bab = BookBab::where('id', $id)->first();
        $bab->delete();
        return redirect()->route('bookbab.index');
    }
}
