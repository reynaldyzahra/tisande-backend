<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;
use App\Http\Resources\UserResource;
use App\User;
class UserController extends Controller
{
    public function getall()
    {
        $user = User::with('profile')
                ->with('topup')
                ->with('roles')
                ->with('order')
                ->get();
        
        return response()->json([
            'status' => 200,
            'message' => 'Successfully get users',
            'data' => UserResource::collection($user)
        ]);
    }

    public function get($id)
    {
        $user = User::with('profile')
                ->with('topup')
                ->with('roles')
                ->with('order')
                ->where('id', $id)
                ->get();
        
        return response()->json([
            'status' => 200,
            'message' => 'Successfully get user',
            'data' => UserResource::collection($user)
        ]);
    }

    public function getorder($id)
    {
        $user = User::with('order.book_bab.books')
                ->where('id', $id)
                ->first();
        
        return response()->json([
            'status' => 200,
            'message' => 'Successfully get user order',
            'data' => $user->order
        ]);
    }


    public function getbooks($id)
    {
        $user = User::with('order.book_bab.books')
                ->where('id', $id)
                ->first();
        
        return response()->json([
            'status' => 200,
            'message' => 'Successfully get user books',
            'data' => OrderResource::collection($user->order)
        ]);
    }
}
