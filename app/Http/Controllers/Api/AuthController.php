<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Spatie\Permission\Models\Role;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $request->validate([
            'username' => 'required|string',
            'email' => 'required|email|string',
            'password' => 'required|string',
            'name' => 'required|string',
        ]);

        //Check role member is exists
        $role_exists = Role::where('name', 'Member')->first();
        if (!isset($role_exists)){
            Role::create([
                'name' => 'Member'
            ]);
        }

        $user = User::create([
            'username' => $request->username,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        //if user create success
        if ($user) {
            $user->profile()->create([
                'name' => $request->name,
                'img_path' => '',
                'saldo' => 150000,
            ]);

            $user->assignRole('Member');

            return response()->json([
                'status' => 201,
                'message' => 'Successfully register user'
            ], 201);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Internal server error!'
            ], 400);
        }
    }
    public function login(Request $request)
    {
        $request->validate([
            'username' => 'required|string',
            'password' => 'required|string',
        ]);

        //Handling user authentications
        $credentials = request(['username', 'password']);
        if (!Auth::attempt($credentials)) {
            return response()->json([
                'status' => 401,
                'message' => 'Username or password is wrong!'
            ], 401);
        }

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;

        $token->save();

        //Get user data
        $user_data = User::where('username', $request->username)
                    ->with('profile')
                    ->with('roles')
                    ->first();
                    
        return response()->json([
            'status' => 200,
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString(),
            'user' => $user_data
        ], 200);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'status' => 200,
            'message' => 'Successfully logout'
        ], 200);
    }


}
