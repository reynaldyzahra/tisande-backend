<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Books;
use App\Http\Resources\BookResource;
use App\User;

class BooksController extends Controller
{
    public function getall()
    {
        $books = Books::with('book_bab')
                ->with('category')
                ->get();

        return BookResource::collection($books);

    }
    public function get($id)
    {
        $book = Books::with('book_bab')
                ->with('category')
                ->where('id', $id)
                ->get();

        return BookResource::collection($book);

    }

    public function getbycategory($id)
    {
        $books = Books::with('book_bab')
                    ->with('category')->get();

        $data_books = [];
        foreach($books as $book) {
            if (count($book->category) > 0) {
                foreach($book->category as $cat) {
                    if ($cat->id == $id) {
                        array_push($data_books, $book);
                        break;
                    }
                }
        
            }
        }

        if ($books) {
            return response()->json([
                "status" => 200,
                "message" => 'Successfully get books',
                "data" => $data_books
            ]);
        }
    }

    public function orderBab($id, $bab_id, Request $request)
    {
        $request->validate([
            'user_id' => 'required|integer'
        ]);

        $bab = Books::where('id', $id)
            ->first()
            ->book_bab
            ->where('id', $bab_id)
            ->first();

        
        $user = User::where('id', $request->user_id)->first();
        //Compare saldo with bab price
        if ($user->profile->saldo >= $bab->price){
            
            $order = $user->order()->create([
                'book_bab_id' => $bab->id,
                'id_order' => ''
            ]);

            $user->profile()->update([
                'saldo' => ($user->profile->saldo - $bab->price)
            ]);

            $order->update([
                'id_order' => str_pad($order->id, 8, '0', STR_PAD_LEFT)
            ]);

            return response()->json([
                'status' => 200,
                'message' => 'Successfully order this bab books'
            ], 200);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Your saldo is not enough'
            ], 400);
        }
    }
}
