<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\TopupResource;
use App\Topup;
use App\User;
use Illuminate\Support\Str;

class TopupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topup = Topup::with('user')->get();
        return TopupResource::collection($topup);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function accept($id)
    {
        $topup = Topup::where('id', $id)->first();
        if ($topup) {
            if ($topup->status == 'Pending') {

                //Update status topup ke sukses
                $topup->update([
                    'status' => 'Success'
                ]);

                //Tambah saldo user
                $topup->user->profile()->update([
                    'saldo' => ($topup->user->profile->saldo + $topup->nominal)
                ]);
    
                return response()->json([
                    'status' => 200,
                    'message' => 'Topup with id ' . $id . ' and amounth Rp' . $topup->nominal . ' has been set to success'
                ], 200);
            } else {
                return response()->json([
                    'status' => 400,
                    'message' => 'Topup status is not in pending!'
                ], 400);
            }
        } else {
            return response()->json([
                'status' => 404,
                'message' => 'Topup not found'
            ], 404);
        }
    }

    public function reject($id)
    {
        $topup = Topup::where('id', $id)->first();
        if ($topup) {
            if ($topup->status != 'Pending') {
                $topup->update([
                    'status' => 'Pending'
                ]);
    
                return response()->json([
                    'status' => 200,
                    'message' => 'Topup with id ' . $id . ' has been set to pending'
                ], 200);
            } else {
                return response()->json([
                    'status' => 400,
                    'message' => 'Topup status is in pending!'
                ], 400);
            }
        } else {
            return response()->json([
                'status' => 404,
                'message' => 'Topup not found'
            ], 404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nominal' => 'required|integer',
            'img_path' => 'required',
            'user_id' => 'required|integer'
        ]);

        $user = User::where('id', $request->user_id)->first();

        if ($request->has('img_path')) {
            $file = $request->file('img_path');
            $imagePath = '/img/topup/';
            $path = public_path() . $imagePath;
            $extension = $file->getClientOriginalExtension();

            //Filename
            $filename = 'topup-'. Str::random(16) . '.' . $extension;
            $file->move($path, $filename);

            $user->topup()->create([
                'nominal' => $request->nominal,
                'img_path' => $filename,
                'status' => 'Pending'
            ]);

            return response()->json([
                'status' => 201,
                'message'=> 'Successfully create topup'
            ], 201);
        } else {
            return response()->json([
                'status' => 400,
                'message'=> 'Please insert image'
            ], 400);
        }
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $topup = Topup::where('id', $id)
                ->with('user')->get();
        return TopupResource::collection($topup);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
