<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Books;
use App\BookBab;
use App\Category;
use Illuminate\Support\Str;

class BooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $books = Books::all();

        return view('adm.books.index', ['books' => $books]);
    }

    public function babindex($id)
    {
        //
        $book = Books::findOrFail($id);
        
        return view('adm.bookbab.index', ['bookBab' => $book->book_bab]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $book = Books::all();
        $category = Category::all();
        return view('adm.books.create', [
            'category'=>$category
        ]);
    }

    public function getbab(Request $request)
    {
        $books = Books::findOrFail($request->id);
        return view('adm.bookbab.selected', [
            'books' => $books
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required|string',
            'sinopsis' => 'required|string',
            'category' => 'required',
            'img_path' => 'required'
        ]);

        if ($request->has('img_path')) {
            $file = $request->file('img_path');
            $imagePath = '/img/books/';
            $path = public_path() . $imagePath;
            $extension = $file->getClientOriginalExtension();

            //Filename
            $filename = 'books-'.Str::random(16) . '.' . $extension;
            $file->move($path, $filename);

            $books = Books::create([
                'name' => $request->name,
                'sinopsis' => $request->sinopsis,
                'category' => $request->category,
                'img_path' => $imagePath . $filename
            ]);
            $books->category()->attach($request->category);
            if ($books) {
                return redirect()->route('books.index');
            }
        }
    }

    public function babstore(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string',
            'price' => 'required|integer',
            'file_path' => 'required',
        ]);

        $books = Books::findOrFail($id);

        if ($request->has('file_path')) {
            $file = $request->file('file_path');
            $content = $file->get();

            //Encrypt Books into .dat file
            $encryptContent = encrypt($content);
            $filename = 'books-'.Str::random(16) . '.dat';
            Storage::disk('local')->put($filename, $encryptContent);

            $books->book_bab()->create([
                'name' => $request->name,
                'price' => $request->price,
                'file_path' => $filename
            ]);

            return redirect()->route('books.index');
        }
   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $books = Books::where('id', $id)->first();
        $books->delete();
        return redirect()->route('books.index');
    }
}
