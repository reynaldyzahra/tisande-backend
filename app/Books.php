<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    protected $table = 'books';
    protected $fillable = [
        'name', 'sinopsis', 'img_path'
    ];

    public function book_bab()
    {
        return $this->hasMany(BookBab::class, 'book_id');
    }

    public function category()
    {
        return $this->belongsToMany(Category::class, 'book_category', 'book_id');
    }
}
