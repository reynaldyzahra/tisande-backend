<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Profile extends Model
{
    protected $table = 'profile';
    protected $fillable = [
        'name', 'img_path', 'saldo'
    ];
    
    public function user() {
        return $this->belongsTo(User::class);
    }
}
