<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';
    protected $fillable = [
        'name','created_ad','updated_at'
    ];


    public function books()
    {
        return $this->belongsToMany(Books::class, 'book_category');
    }
}
