<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/  


// Route::get('/dashboard', function() {
//     return view('dashboard');
// })->middleware('guest')->name('home');
Auth::routes();

    Route::get('/dashboard', 'HomeController@index')->middleware('auth')->name('dashboard');
    Route::get('/', function() {
        return view('welcome'); 
    });
    Route::group(['middleware' => ['auth']], function () {
        
        Route::resource('bookbab', 'BookBabController');
        Route::resource('books', 'BooksController');
        Route::get('books/{id}', 'BooksController@babindex');
        
        //Pengen tau liat semua bab books
        Route::get('books/{id}/bab', 'BooksController@getbab');
        
        Route::resource('category', 'CategoryController');
        // Route::resource('topup', 'Api\TopupController');
        Route::resource('topups', 'TopupsController');
        Route::get('topup/{id}/accept', 'TopupsController@accept');
        Route::get('topup/{id}/reject', 'TopupsController@reject')->name('topups.reject');
        // Route::post('topup/{id}/reject', 'Api\TopupController@reject');
        
            
    });