<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'user'
], function () {
    Route::post('login', 'Api\AuthController@login');
    Route::post('register', 'Api\AuthController@register');
  
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'Api\AuthController@logout');

        Route::get('/', 'Api\UserController@getall');
        Route::get('/{id}', 'Api\UserController@get');
        Route::get('/{id}/order', 'Api\UserController@getorder');
        Route::get('/{id}/books', 'Api\UserController@getbooks');
    });
});

Route::group([
    'prefix' => 'books'
], function() {
    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('/', 'Api\BooksController@getall');
        Route::get('/{id}', 'Api\BooksController@get');

        Route::get('/category/{id}', 'Api\BooksController@getbycategory');
        
        Route::post('/{id}/bab/{bab_id}/order', 'Api\BooksController@orderBab');
    });
});

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::resource('topup', 'Api\TopupController');
        Route::post('topup/{id}/accept', 'Api\TopupController@accept');
        Route::post('topup/{id}/reject', 'Api\TopupController@reject');
    });