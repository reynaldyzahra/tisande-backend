<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>One Page Navigation CSS Menu</title>
  <meta name="designer" content="Alberto Hartzet">
<meta name="programer" content="Alberto Hartzet">
<meta name="author" content="Alberto Hartzet">
<meta name="description" content="One page navigation with pure CSS">
<meta property="og:url" content="https://codepen.io/hrtzt/details/NPZKRN">
<meta property="og:image" content="https://pbs.twimg.com/media/CCNJN_XUMAAJSzU.jpg:large">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.css'>
<link rel="stylesheet" href="{{asset('css/style.css')}}"><script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>

</head>
<body>
<!-- partial:index.partial.html -->
<div class="ct" id="t1">
  <div class="ct" id="t2">
    <div class="ct" id="t3">
      <div class="ct" id="t4">
         <div class="ct" id="t5">
          <ul id="menu">
            <a href="#t1"><li class="icon fa fa-home" id="uno"></li></a>
            <a href="#t2"><li class="icon fa fa-book" id="dos"></li></a>
            <a href="#t3"><li class="icon fa fa-user" id="tres"></li></a>
          </ul>
          <div class="page" id="p1">
             <section class="icon">
             <img style="width: 300px;height: 150px;" src="{{asset('img/logologin.png')}}" alt=""> 
              {{-- <span class="title">Tisande</span> --}}
              <span class="hint">Platform pembelajaran untuk anak dengan tampilan menarik dan modern <br> dengan disertai buku pembelajaran yang bagus untuk mendukung pendidikan anak </span></section>  
          </div>
          <div class="page" id="p2">
            <section class="icon fa fa-book"><span class="title">Tisande</span>
            <span class="hint">Tersedia untuk platform Mobile Android</span>
            </section>
          </div>  
          <div class="page" id="p3">
            <section class="icon fa fa-user"><span class="title">Rocket</span></section>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- partial -->
<script  src="{{asset('js/script.js')}}"></script>

</body>
</html>